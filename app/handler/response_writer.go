package handler

import (
	"encoding/json"
	"net/http"

	"gitlab.com/ldath-core/examples/ex-book-admin-api-go-mux/app/model"
)

// ResponseWriter will write result in http.ResponseWriter
func ResponseWriter(res http.ResponseWriter, statusCode int, message string, data interface{}) error {
	res.WriteHeader(statusCode)
	httpResponse := model.NewResponse(statusCode, message, data)
	err := json.NewEncoder(res).Encode(httpResponse)
	return err
}

// PaginatedResponseWriter will write result in http.ResponseWriter
func PaginatedResponseWriter(res http.ResponseWriter, statusCode int, count, skip, limit int64, message string, data interface{}) error {
	res.WriteHeader(statusCode)
	httpResponse := model.NewPaginatedResponse(statusCode, count, skip, limit, message, data)
	err := json.NewEncoder(res).Encode(httpResponse)
	return err
}
