package handler

import (
	"context"
	"net/http"
	"time"

	"gitlab.com/ldath-core/examples/ex-book-admin-api-go-mux/app/model"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

// GetHealth will handle health get request
func (c *Controller) GetHealth(res http.ResponseWriter, _ *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()

	// Ping the primary
	m := true
	if err := c.MDB.Client().Ping(ctx, readpref.Primary()); err != nil {
		m = false
	}

	err := ResponseWriter(res, http.StatusOK, "ex-book-admin-api-go api health", model.Health{
		Alive: true,
		Mongo: m,
	})
	if err != nil {
		c.Logger.Error(err)
	}
}
