package handler

import (
	"context"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/mux"
	"gitlab.com/ldath-core/examples/ex-book-admin-api-go-mux/app/db"
	"gitlab.com/ldath-core/examples/ex-book-admin-api-go-mux/config"
)

const succeed = "\u2713"
const failed = "\u2717"

type CreateTestData struct {
	DropCollection bool
	LoadTestData   bool
}

func createNewRecorder(r *mux.Router, method, endpoint string, qData map[string]string, body io.Reader) *httptest.ResponseRecorder {
	req, _ := http.NewRequest(method, endpoint, body)
	if len(qData) >= 0 {
		q := req.URL.Query()
		for k, v := range qData {
			q.Add(k, v)
		}
		req.URL.RawQuery = q.Encode()
	}

	rr := httptest.NewRecorder()
	r.ServeHTTP(rr, req)
	return rr
}

func setup(t *testing.T, ctx context.Context, createTestData CreateTestData) Controller {
	l, d, c := config.GetTestSetup(t, ctx)
	controller := Controller{
		MDB:    d,
		Logger: l,
		Config: c,
	}

	if createTestData.DropCollection {
		// Drop all prior to create test data
		err := db.DropAllCollections(ctx, d, l)
		if err != nil {
			t.Fatal(err)
		}

		// Migrate Database to the latest version
		err = db.MigrateDatabase(ctx, d, l)
		if err != nil {
			t.Fatal(err)
		}
	}

	if createTestData.LoadTestData && createTestData.DropCollection {
		// Load Test DATA
		err := db.CreateTestData(ctx, d, l)
		if err != nil {
			t.Fatal(err)
		}
	}
	return controller
}

func testResponseStatusCheck(t *testing.T, expectedStatusCode int, rr *httptest.ResponseRecorder) {
	if status := rr.Code; status != expectedStatusCode {
		t.Errorf("%s check StatusCreated is failed: got %d want %d", failed, status, expectedStatusCode)
		t.Errorf("BODY: %s", rr.Body)
	} else {
		t.Logf("%s check status %d is successfull.", succeed, expectedStatusCode)
	}
}
