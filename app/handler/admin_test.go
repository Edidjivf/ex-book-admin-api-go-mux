package handler

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"testing"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/ldath-core/examples/ex-book-admin-api-go-mux/app/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

func TestCreateAdmin(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{DropCollection: true})
	e, _ := json.Marshal(model.CreateAdminInput{
		Email:     "test@localhost.com",
		FirstName: "Test",
		LastName:  "User",
		Password:  "testpass4U",
	})

	r := mux.NewRouter()
	r.HandleFunc("/v1/book-admins", c.CreateAdmin)

	// check http created status
	rr := createNewRecorder(r, "POST", "/v1/book-admins", nil, bytes.NewBuffer(e))
	testResponseStatusCheck(t, http.StatusCreated, rr)

	defer func() {
		if e := c.MDB.Client().Disconnect(ctx); e != nil {
			t.Fatal(e)
		}
	}()
}

func TestCreateAdminWithoutPassword(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{DropCollection: true})
	e, _ := json.Marshal(model.CreateAdminInput{
		Email:     "test@localhost.com",
		FirstName: "Test",
		LastName:  "User",
	})

	r := mux.NewRouter()
	r.HandleFunc("/v1/book-admins", c.CreateAdmin)

	// check http created status
	rr := createNewRecorder(r, "POST", "/v1/book-admins", nil, bytes.NewBuffer(e))
	testResponseStatusCheck(t, http.StatusBadRequest, rr)

	defer func() {
		if e := c.MDB.Client().Disconnect(ctx); e != nil {
			t.Fatal(e)
		}
	}()
}

func TestCreateAdminWithoutEmail(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{DropCollection: true})
	e, _ := json.Marshal(model.CreateAdminInput{
		FirstName: "Test",
		LastName:  "User",
		Password:  "testpass4U",
	})

	r := mux.NewRouter()
	r.HandleFunc("/v1/book-admins", c.CreateAdmin)

	// check http created status
	rr := createNewRecorder(r, "POST", "/v1/book-admins", nil, bytes.NewBuffer(e))
	testResponseStatusCheck(t, http.StatusBadRequest, rr)

	defer func() {
		if e := c.MDB.Client().Disconnect(ctx); e != nil {
			t.Fatal(e)
		}
	}()
}

func TestCreateAdminWithEmptyEmailAndPassword(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{DropCollection: true})
	e, _ := json.Marshal(model.CreateAdminInput{
		FirstName: "Test",
		LastName:  "User",
		Password:  "",
		Email:     "",
	})

	r := mux.NewRouter()
	r.HandleFunc("/v1/book-admins", c.CreateAdmin)

	// check http created status
	rr := createNewRecorder(r, "POST", "/v1/book-admins", nil, bytes.NewBuffer(e))
	testResponseStatusCheck(t, http.StatusBadRequest, rr)

	defer func() {
		if e := c.MDB.Client().Disconnect(ctx); e != nil {
			t.Fatal(e)
		}
	}()
}

func TestCreateAdminWhenAlreadyExist(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{DropCollection: true})
	e, _ := json.Marshal(model.CreateAdminInput{
		Email:     "test@localhost.com",
		FirstName: "Test",
		LastName:  "User",
		Password:  "testpass4U",
	})

	r := mux.NewRouter()
	r.HandleFunc("/v1/book-admins", c.CreateAdmin)

	// check http created status
	rr := createNewRecorder(r, "POST", "/v1/book-admins", nil, bytes.NewBuffer(e))
	testResponseStatusCheck(t, http.StatusCreated, rr)

	rr = createNewRecorder(r, "POST", "/v1/book-admins", nil, bytes.NewBuffer(e))
	testResponseStatusCheck(t, http.StatusNotAcceptable, rr)

	defer func() {
		if e := c.MDB.Client().Disconnect(ctx); e != nil {
			t.Fatal(e)
		}
	}()
}

func TestGetAdmins(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{DropCollection: true, LoadTestData: true})

	r := mux.NewRouter()
	r.HandleFunc("/v1/book-admins", c.GetAdmins)

	rr := createNewRecorder(r, "GET", "/v1/book-admins", nil, nil)
	testResponseStatusCheck(t, http.StatusOK, rr)

	rr = createNewRecorder(r, "GET", "/v1/book-admins", map[string]string{"page": "0"}, nil)
	testResponseStatusCheck(t, http.StatusOK, rr)

	defer func() {
		if e := c.MDB.Client().Disconnect(ctx); e != nil {
			t.Fatal(e)
		}
	}()
}

func TestGetAdmin(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{DropCollection: true, LoadTestData: true})

	r := mux.NewRouter()
	r.HandleFunc("/v1/book-admins/{id}", c.GetAdmin)

	var admin model.Admin
	err := c.MDB.Collection("admin").FindOne(ctx, bson.M{}).Decode(&admin)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			t.Error("There is 0 documents in the collection - fix tests")
			return
		}
		t.Error(err)
		return
	}

	rr := createNewRecorder(r, "GET", fmt.Sprintf("/v1/book-admins/%s", admin.ID.Hex()), nil, nil)
	testResponseStatusCheck(t, http.StatusOK, rr)

	defer func() {
		if e := c.MDB.Client().Disconnect(ctx); e != nil {
			t.Fatal(e)
		}
	}()
}

func TestGetAdminViaEmail(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{DropCollection: true, LoadTestData: true})

	r := mux.NewRouter()
	r.HandleFunc("/v1/book-admins", c.GetAdmins)

	rr := createNewRecorder(r, "GET", "/v1/book-admins", nil, nil)
	testResponseStatusCheck(t, http.StatusOK, rr)

	var admin model.Admin
	err := c.MDB.Collection("admin").FindOne(ctx, bson.M{}).Decode(&admin)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			t.Error("There is 0 documents in the collection - fix tests")
			return
		}
		t.Error(err)
		return
	}

	rr = createNewRecorder(r, "GET", fmt.Sprintf("/v1/book-admins?email=%s", admin.Email), nil, nil)
	testResponseStatusCheck(t, http.StatusOK, rr)

	defer func() {
		if e := c.MDB.Client().Disconnect(ctx); e != nil {
			t.Fatal(e)
		}
	}()
}

func TestGetAdminUsingQueryWithLimitSetToZero(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{DropCollection: true, LoadTestData: true})

	r := mux.NewRouter()
	r.HandleFunc("/v1/book-admins", c.GetAdmins)

	rr := createNewRecorder(r, "GET", "/v1/book-admins", nil, nil)
	testResponseStatusCheck(t, http.StatusOK, rr)

	var admin model.Admin
	err := c.MDB.Collection("admin").FindOne(ctx, bson.M{}).Decode(&admin)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			t.Error("There is 0 documents in the collection - fix tests")
			return
		}
		t.Error(err)
		return
	}

	rr = createNewRecorder(r, "GET", "/v1/book-admins?limit=0", nil, nil)
	testResponseStatusCheck(t, http.StatusNotFound, rr)

	defer func() {
		if e := c.MDB.Client().Disconnect(ctx); e != nil {
			t.Fatal(e)
		}
	}()
}

func TestGetAdminUsingQueryWithLimitSetToOne(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{DropCollection: true, LoadTestData: true})

	r := mux.NewRouter()
	r.HandleFunc("/v1/book-admins", c.GetAdmins)

	rr := createNewRecorder(r, "GET", "/v1/book-admins", nil, nil)
	testResponseStatusCheck(t, http.StatusOK, rr)

	var admin model.Admin
	err := c.MDB.Collection("admin").FindOne(ctx, bson.M{}).Decode(&admin)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			t.Error("There is 0 documents in the collection - fix tests")
			return
		}
		t.Error(err)
		return
	}

	rr = createNewRecorder(r, "GET", "/v1/book-admins?limit=1", nil, nil)
	testResponseStatusCheck(t, http.StatusOK, rr)

	defer func() {
		if e := c.MDB.Client().Disconnect(ctx); e != nil {
			t.Fatal(e)
		}
	}()
}

func TestUpdateAdmin(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{DropCollection: true, LoadTestData: true})

	r := mux.NewRouter()
	r.HandleFunc("/v1/book-admins/{id}", c.UpdateAdmin)

	var admin model.Admin
	err := c.MDB.Collection("admin").FindOne(ctx, bson.M{}).Decode(&admin)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			t.Error("There is 0 documents in the collection - fix tests")
			return
		}
		t.Error(err)
		return
	}

	updateAdmin := map[string]interface{}{"email": "test.update@localhost.org"}
	e, _ := json.Marshal(updateAdmin)

	rr := createNewRecorder(r, "PUT", fmt.Sprintf("/v1/book-admins/%s", admin.ID.Hex()), nil, bytes.NewBuffer(e))
	testResponseStatusCheck(t, http.StatusAccepted, rr)

	defer func() {
		if e := c.MDB.Client().Disconnect(ctx); e != nil {
			t.Fatal(e)
		}
	}()
}

func TestDeleteAdmin(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{DropCollection: true, LoadTestData: true})

	r := mux.NewRouter()
	r.HandleFunc("/v1/book-admins/{id}", c.DeleteAdmin)

	var admin model.Admin
	err := c.MDB.Collection("admin").FindOne(ctx, bson.M{}).Decode(&admin)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			t.Error("There is 0 documents in the collection - fix tests")
			return
		}
		t.Error(err)
		return
	}

	rr := createNewRecorder(r, "DELETE", fmt.Sprintf("/v1/book-admins/%s", admin.ID.Hex()), nil, nil)
	testResponseStatusCheck(t, http.StatusAccepted, rr)

	defer func() {
		if e := c.MDB.Client().Disconnect(ctx); e != nil {
			t.Fatal(e)
		}
	}()
}
