package handler

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"gitlab.com/ldath-core/examples/ex-book-admin-api-go-mux/app/model"

	"github.com/go-playground/validator/v10"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"golang.org/x/crypto/bcrypt"
	"golang.org/x/net/context"
)

// CreateAdmin will handle the create Admin post request
func (c *Controller) CreateAdmin(res http.ResponseWriter, req *http.Request) {
	preAdmin := new(model.CreateAdminInput)

	err := json.NewDecoder(req.Body).Decode(preAdmin)
	if err != nil {
		err = ResponseWriter(res, http.StatusBadRequest, "body json request have issues!!!", nil)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}

	validate := validator.New()
	err = validate.Struct(preAdmin)
	if err != nil {
		response_body := "body json request have issues!!!"
		if strings.Contains(err.Error(), "'Password'") {
			response_body += "Password cannot be empty or not defined. "
		}

		if strings.Contains(err.Error(), "'Email'") {
			response_body += "Email cannot be empty/not defiend/or have incorrect structure. "
		}

		err = ResponseWriter(res, http.StatusBadRequest, response_body, nil)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}

	admin, err := model.NewAdmin(preAdmin.Email, preAdmin.FirstName, preAdmin.LastName, preAdmin.Password)
	if err != nil {
		c.Logger.Error(err)
		err = ResponseWriter(res, http.StatusInternalServerError, "there is an error on server!!!", nil)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}
	result, err := c.MDB.Collection("admin").InsertOne(req.Context(), admin)
	if err != nil {
		switch err.(type) {
		case mongo.WriteException:
			err = ResponseWriter(res, http.StatusNotAcceptable, "username or email already exists in database.", nil)
		default:
			err = ResponseWriter(res, http.StatusInternalServerError, "Error while inserting data.", nil)
		}
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}
	admin.ID = result.InsertedID.(primitive.ObjectID)
	err = ResponseWriter(res, http.StatusCreated, fmt.Sprintf("admin: %s created", admin.ID.Hex()), admin)
	if err != nil {
		c.Logger.Error(err)
	}
}

// GetAdmins will handle people list get request
func (c *Controller) GetAdmins(res http.ResponseWriter, req *http.Request) {
	var adminList []model.Admin
	skipString := req.FormValue("skip")
	skip, err := strconv.ParseInt(skipString, 10, 64)
	if err != nil {
		skip = 0
	}
	limitString := req.FormValue("limit")
	limit, err := strconv.ParseInt(limitString, 10, 64)
	if err != nil {
		limit = 10
	}

	if limit == 0 {
		c.Logger.Printf("Invalid value of limit: %v\n", limit)
		err = ResponseWriter(res, http.StatusNotFound, "Invalid value of limit key", nil)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}

	emailString := req.FormValue("email")
	filter := bson.M{}
	if emailString != "" {
		filter["email"] = emailString
	}

	countOptions := options.CountOptions{}
	count, err := c.MDB.Collection("admin").CountDocuments(req.Context(), filter, &countOptions)
	if err != nil {
		c.Logger.Printf("Error while counting collection: %v\n", err)
		err = ResponseWriter(res, http.StatusInternalServerError, "Error occurred while reading data", nil)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}

	findOptions := options.FindOptions{
		Skip:  &skip,
		Limit: &limit,
		Sort: bson.M{
			"_id": -1, // -1 for descending and 1 for ascending
		},
	}
	cursor, err := c.MDB.Collection("admin").Find(req.Context(), filter, &findOptions)
	if err != nil {
		c.Logger.Printf("Error while quering collection: %v\n", err)
		err = ResponseWriter(res, http.StatusInternalServerError, "Error occurred while reading data", nil)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}
	err = cursor.All(context.Background(), &adminList)
	if err != nil {
		c.Logger.Fatalf("Error in cursor: %v", err)
		err = ResponseWriter(res, http.StatusInternalServerError, "Error occurred while reading data", nil)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}
	err = PaginatedResponseWriter(res, http.StatusOK, count, skip, limit, fmt.Sprintf("admins - skip: %d; limit: %d", skip, limit), adminList)
	if err != nil {
		c.Logger.Error(err)
	}
}

// GetAdmin will give us admin with special id
func (c *Controller) GetAdmin(res http.ResponseWriter, req *http.Request) {
	var params = mux.Vars(req)
	id, err := primitive.ObjectIDFromHex(params["id"])
	if err != nil {
		err = ResponseWriter(res, http.StatusBadRequest, fmt.Sprintf("id: %s is wrong", params["id"]), nil)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}
	var admin model.Admin
	err = c.MDB.Collection("admin").FindOne(req.Context(), model.Admin{ID: id}).Decode(&admin)
	if err != nil {
		switch err {
		case mongo.ErrNoDocuments:
			err = ResponseWriter(res, http.StatusNotFound, "admin not found for given id", nil)
		default:
			c.Logger.Printf("Error while decode to go struct:%v\n", err)
			err = ResponseWriter(res, http.StatusInternalServerError, "there is an error on server!!!", nil)
		}
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}
	err = ResponseWriter(res, http.StatusOK, "admin", admin)
}

// UpdateAdmin will handle the admin update endpoint
func (c *Controller) UpdateAdmin(res http.ResponseWriter, req *http.Request) {
	var updateData map[string]interface{}
	err := json.NewDecoder(req.Body).Decode(&updateData)
	if err != nil {
		err = ResponseWriter(res, http.StatusBadRequest, "json body is incorrect", nil)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}
	// we don't handle the json decode return error because all our fields have the omitempty tag.
	var params = mux.Vars(req)
	oid, err := primitive.ObjectIDFromHex(params["id"])
	if err != nil {
		err = ResponseWriter(res, http.StatusBadRequest, "id that you sent is wrong!!!", nil)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}

	if val, keyExist := updateData["password"]; keyExist {
		byteArray, err := bcrypt.GenerateFromPassword([]byte(val.(string)), 14)
		if err != nil {
			err = ResponseWriter(res, http.StatusBadRequest, "Error during updating password !!!", nil)
			if err != nil {
				c.Logger.Error(err)
			}
			return
		}
		delete(updateData, "password")
		updateData["passwordHash"] = bytes.NewBuffer(byteArray).String()
	}

	update := bson.M{
		"$set": updateData,
	}

	result, err := c.MDB.Collection("admin").UpdateOne(req.Context(), model.Admin{ID: oid}, update)
	if err != nil {
		c.Logger.Printf("Error while updateing document: %v", err)
		err = ResponseWriter(res, http.StatusInternalServerError, "error in updating document!!!", nil)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}
	if result.MatchedCount == 1 {
		err = ResponseWriter(res, http.StatusAccepted, fmt.Sprintf("admin: %s updated", oid.Hex()), &updateData)
	} else {
		err = ResponseWriter(res, http.StatusNotFound, "admin not found", nil)
	}
	if err != nil {
		c.Logger.Error(err)
	}
}

func (c *Controller) DeleteAdmin(res http.ResponseWriter, req *http.Request) {
	var params = mux.Vars(req)
	id, err := primitive.ObjectIDFromHex(params["id"])
	if err != nil {
		err = ResponseWriter(res, http.StatusBadRequest, "id that you sent is wrong!!!", nil)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}
	result, err := c.MDB.Collection("admin").DeleteOne(req.Context(), model.Admin{ID: id})
	if err != nil {
		c.Logger.Printf("Error while deleting document: %v", err)
		err = ResponseWriter(res, http.StatusInternalServerError, "error in deleting document!!!", nil)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}
	if result.DeletedCount == 1 {
		err = ResponseWriter(res, http.StatusAccepted, fmt.Sprintf("admin: %s deleted", id.Hex()), nil)
	} else {
		err = ResponseWriter(res, http.StatusNotFound, "admin not found", nil)
	}
	if err != nil {
		c.Logger.Error(err)
	}
}
